<?php 
require_once('connexion.php');
require_once('getGenres.php');
?>


<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>
        Formulaire d'entrée en BD
        </title>
    </head>

<body>
    
    <h1> Formulaire d'entrée en BD </h1>
    <form name="quizz" method="POST" action="./addFilm.php">

    <p>
        titre_original
            <br/>
            <input type="text" name="titre_original">
            </p>
        <p>
            
        <p>
        titre_francais
            <br/>
            <input type="text" name="titre_francais">
            </p>
        <p>
            
        <p>
        realisateur
            <br/>
            <input type="text" name="realisateur">
            </p>
        <p>
            
        <p>
        pays
            <br/>
            <input type="text" name="pays">
            </p>
        <p>
            
        <p>
        duree
            <br/>
            <input type="text" name="duree">
            </p>
        <p>
            
        <p>
        couleur
            <br/>
            <input type="text" name="couleur">
            </p>
        <p>
            
        <p>
        date
            <br/>
            <input type="text" name="date">
            </p>
        <p>
            
        <p>
        nom_genre
        <?php
        $liste = getGenres(connect_bd());
        foreach ($liste as $genre){ ?>
            <br/>
            <input type="checkbox" name="nom_genre[]" value="<?php echo $genre["code_genre"];?>">
        <?php echo $genre["nom_genre"];}?>
            </p>
        <p>
            
        <p>
        image
            <br/>
            <input type="text" name="image">
            </p>
        <p>
            
        <input type="submit" value ="Répondre">
        <br/>
        <input type="reset" value="Recommencer">
    </form>
    </body>
</html>
