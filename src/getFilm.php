<?php
require_once('connexion.php');

function rechercheFilms($connexion,$titre,$real,$annee,$genre){
// Les 2 identifiants sont-ils transmis ?
        // Est-ce que l'utilisateur est dans la base ?
    $sql="SELECT distinct code_film,titre_original,titre_francais,pays,date,duree,couleur,realisateur,image FROM films inner join classification on code_film=ref_code_film";
    if(!empty($titre) || !empty($real) || !empty($annee) || !empty($genre)){
        $sql = $sql." where ";
        $already = False;
        if(!empty($titre)){
            $already = True;
            $sql = $sql."(titre_original like '%".$titre."%' or titre_francais like '%".$titre."%')";
        }
        if(!empty($real)){
            if($already){
                $sql = $sql." and ";
            }
            $sql = $sql."realisateur = :real";
        }
        if(!empty($annee)){
            if($already){
                $sql = $sql." and ";
            }
            $sql = $sql."date = :annee";
        }
        if(!empty($genre)){
            if($already){
                $sql = $sql." and ";
            }
            if(is_array($genre)){
                $alreadybis = False;
                foreach($genre as $g){
                    if($alreadybis){
                        $sql = $sql." or ";
                    }
                    else{
                        $sql = $sql."(";
                        $alreadybis = True;
                    }
                    $sql = $sql."ref_code_genre = ".$g;
                }
                $sql = $sql.")";
            }
            else{
                $sql = $sql."ref_code_genre =".$genre;
            }
        }
    }
    $stmt = $connexion->prepare($sql);
    if(!empty($real)){
        $stmt->bindParam(':real', $real);
    }
    if(!empty($annee)){
        $stmt->bindParam(':annee', $annee);
    }
    $stmt->execute();
    if($stmt->rowCount() > 0){
        $liste = array();
        while ($row =$stmt->fetch()){
            array_push($liste,$row);
        }
        //$liste = trie($titre,$real,$annee,$genre,$liste);
        //foreach($liste as $row){
        //    echo $row["titre_original"]."   ".$row["titre_francais"]."\n";
        //}
        return $liste;
	}
    else{
    echo "erreur";
    }
    return null;
}

function getFilmAccueil($connexion){
    return rechercheFilms($connexion,null,null,null,null);
}

function getGenre($connexion,$code_film){
    $sql = "select distinct nom_genre, code_genre from classification inner join genres on code_genre=ref_code_genre where ref_code_film=".$code_film;
    $liste = array();
    foreach($connexion->query($sql) as $row){
        array_push($liste,$row);
    }
    return $liste;
  }

function getFilmById($connexion,$code_film){
    $sql = "SELECT * from films where code_film=".$code_film;
    $liste = array();
    foreach($connexion->query($sql) as $row){
        array_push($liste,$row);
    }
    return $liste;
}
function getRealById($connexion,$idReal){
    $sql = "SELECT * from individus where code_indiv=".$idReal;
    $liste = array();
    foreach($connexion->query($sql) as $row){
        array_push($liste,$row);
    }
    return $liste;

}
