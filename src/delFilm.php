<?php

require_once('connexion.php');

$connexion=connect_bd();

$errorMessage = '';

if(!empty($_POST['id']))
{
	$sql="SELECT * FROM films where code_film=:id";
	$stmt = $connexion->prepare($sql);
	$stmt->bindParam(':id', $_POST['id']);
	$stmt->execute();

 if($stmt->rowCount() > 0)
 	{

	$sql="DELETE FROM films where code_film=:id";
	$stmt = $connexion->prepare($sql);
	$stmt->bindParam(':id', $_POST['id']);
	$stmt->execute();

	}
 else{
	 $errorMessage = 'Le film n\'est pas dans la base';
	 echo $errorMessage;
 }
}
header('Location: ../page/gere.php');
