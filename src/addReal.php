<?php

require_once('connexion.php');

$connexion=connect_bd();

$errorMessage = '';

if(!is_numeric($_POST['naiss'])){
	$errorMessage = "La date n'est pas au bon format (année en chiffres)";
	echo $errorMessage;
}
if(!is_numeric($_POST['mort'])){
	$errorMessage = "La date n'est pas au bon format (année en chiffres)";
	echo $errorMessage;
}

$sql = "SELECT code_indiv from individus where nom = :nom, prenom = :prenom, nationalite = :nat, date_naiss = :naiss, date_mort = :mort";
if($_POST["mort"]==NULL){
    $mort = 0;
}
else{
    $mort =$_POST["mort"]; 
}
$stmt = $connexion->prepare($sql);
$stmt->bindParam(':nom', $_POST['NReal']);
$stmt->bindParam(':prenom', $_POST['PReal']);
$stmt->bindParam(':nat', $_POST['nat']);
$stmt->bindParam(':naiss', $_POST['naiss']);
$stmt->bindParam(':mort', $mort);
$stmt->execute();
if($stmt->rowCount() == 0){
    $sql = "INSERT INTO `individus` (`code_indiv`,`nom`,`prenom`,`nationalite`,`date_naiss`,`date_mort`) VALUES (NULL,:nom,:prenom,:nat,:naiss,:mort)" ;
    $stmt = $connexion->prepare($sql);
    $stmt->bindParam(':nom', $_POST['NReal']);
    $stmt->bindParam(':prenom', $_POST['PReal']);
    $stmt->bindParam(':nat', $_POST['nat']);
    $stmt->bindParam(':naiss', $_POST['naiss']);
    $stmt->bindParam(':mort', $mort);
    $stmt->execute();
    echo $connexion->lastInsertId();
    header('Location: ../page/gere.php');
}
else{
	$errorMessage = "Le réalisateur est déjà dans la base";
    echo $errorMessage;
}