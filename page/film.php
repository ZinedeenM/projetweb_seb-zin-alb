<?php
  require_once('../src/getFilm.php');
  require_once('../src/getGenres.php');
  $connexion = connect_bd() ?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Gerer les films</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="../assets/css/css.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  </head>
  <header>
    <ul class="nav ulNav">
      <li class="nav-item"> <a href="accueil.php"> <img src="../assets/img/iut.png" alt="" class="logoIut"> </a>  </li>
      <li class="nav-item liNav"> <button data-toggle="collapse" href="#collapseForm" aria-expanded="false" aria-controls="collapseForm" class="btn btn-primary">Rechercher</button> </li>
      <li class="liNav collapse" id="collapseForm" style="max-width:50%;">
        <form method='post' class="form-inline" action="accueil.php">
          <input type="text" name="Title" placeHolder="Titre de votre film" class="form-group">
          <div id="TypeResearch" class="form-group divGenre">
            <?php
            $genrelist = getGenres($connexion);
            foreach ($genrelist as $key => $value){ ?>
              <label for=""><input type="checkbox" name="Genre[]" value=<?php echo str_replace(" ", "",$value['nom_genre'])?>> <?php echo $value['nom_genre'] ?> </label>
            <?php } ?>
          </div>
          <input type="submit" value="Rechercher" class="form-group">
        </form>
        <li>
          <li class="nav-item liNav"> <a href="gere.php" class="btn btn-primary">Gérer Films</a> </li>
        </ul>
      </header>
      <?php
        $film = getFilmById($connexion, $_GET['idF'])[0];
        $real = getRealById($connexion, $film['realisateur'])[0];
       ?>
      <body>
        <div class="" style="display:flex; flex-wrap:wrap;">
          <img src="../assets/img/<?php echo $film['image'] ?>" alt="" style="flex:1 1 auto; max-width:40%; margin-right:20%;">
          <aside class="card card-body" style="flex:1 1 auto; max-width:30%;">
            <h3 class="card-title"><?php echo $film['titre_original'] ?></h3>
            <h5 class="card-title"><?php echo $film['titre_francais']; ?></h5>
            <p class="card-text">Crée en : <?php echo $film['pays'] ?></p>
            <p class="card-text">En : <?php echo $film['date'] ?></p>
            <p class="card-text">D'une durée de : <?php echo $film['duree'] ?></p>
            <p class="card-text">Un film en <?php echo $couleur ?? "couleur" ?></p>
            <p class="card-text">Par : <?php echo $real['prenom']." ".$real['nom'] ?></p>
          </aside>
        </div>
        <div class="">
          <p>Genre :</p>
          <?php $genrefilm = getGenre($connexion,$_GET['idF']);
          foreach ($genrefilm as $genre): ?>
            <p><a href="accueil.php?genre=<?php echo $genre['code_genre']; ?>"><?php echo $genre['nom_genre']; ?></a></p>
          <?php endforeach; ?>
        </div>


        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      </body>
