<?php
  require_once('../src/getFilm.php');
  require_once('../src/getGenres.php');
  $connexion = connect_bd() ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
      <head>
        <meta charset="utf-8">
        <title>Collection film</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="../assets/css/css.css">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  </head>
  <header>
        <ul class="nav ulNav">
          <li class="nav-item"> <a href="accueil.php"> <img src="../assets/img/iut.png" alt="" class="logoIut"> </a>  </li>
          <li class="nav-item liNav"> <button data-toggle="collapse" href="#collapseForm" aria-expanded="false" aria-controls="collapseForm" class="btn btn-primary">Rechercher</button> </li>
          <li class="liNav collapse" id="collapseForm" style="max-width:50%;">
              <form method='post' class="form-inline" action="accueil.php">
                <input type="text" name="Title" placeHolder="Titre de votre film" class="form-group">
                <div id="TypeResearch" class="form-group divGenre">
                  <?php
                  $genrelist = getGenres($connexion);
                  foreach ($genrelist as $key => $value){ ?>
                    <label for=""><input type="checkbox" name="nom_genre[]" value="<?php echo $value['code_genre']?>"> <?php echo $value['nom_genre'] ?> </label>
                  <?php } ?>
                </div>
                <input type="submit" value="Rechercher" class="form-group">
              </form>
          <li>
          <li class="nav-item liNav"> <a href="gere.php" class="btn btn-primary">Gérer Films</a> </li>
    </ul>
  </header>
  <body>
    <div class="divBody">
      <?php
      $filmList = rechercheFilms($connexion, $_POST['Title'] ?? null, $_POST['Real'] ?? null, $_POST['annee'] ?? null, $_POST['nom_genre'] ?? $_GET['genre'] ?? null);
      if (!is_null($filmList)) {
        foreach ($filmList as $key => $value) { ?>
        <div class="card divCard" style="max-width : 20%" onloadedmetadata="">
          <a href="film.php?idF=<?php echo $value['code_film'] ?>" style="min-width:100%;">
            <img src="<?php echo "../assets/img/".$value['image']; ?>" alt="Image Poster" style="max-height:200px; min-width:100%;">
          </a>
          <div class="card-body">
            <h5 class="card-title"><?php echo $value['titre_original']; ?></h5>
            <h6 class="card-title"><?php echo $value['titre_francais']; ?></h6>
            <p class="card-text">genre :  </p>
            <?php
            $listeGenre = getGenre($connexion,$value["code_film"]);
            foreach ($listeGenre as $genre) {
            ?>
            <p><a href="?genre=<?php echo $genre['code_genre']; ?>"><?php echo $genre['nom_genre']; ?></a></p>
            <?php } ?>
          </div>
        </div>
      <?php
    }}else {
      echo " : Il n'y a pas de film correspondant à votre recherche";
    }
       ?>
    </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
