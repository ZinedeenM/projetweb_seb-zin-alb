<?php
  require_once('../src/getFilm.php');
  require_once('../src/getGenres.php');
  $connexion = connect_bd() ?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Gerer les films</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="../assets/css/css.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  </head>
  <header>
    <ul class="nav ulNav">
      <li class="nav-item"> <a href="accueil.php"> <img src="../assets/img/iut.png" alt="" class="logoIut"> </a>  </li>
      <li class="nav-item liNav"> <button data-toggle="collapse" href="#collapseForm" aria-expanded="false" aria-controls="collapseForm" class="btn btn-primary">Rechercher</button> </li>
      <li class="liNav collapse" id="collapseForm" style="max-width:50%;">
        <form method='post' class="form-inline" action="accueil.php">
          <input type="text" name="Title" placeHolder="Titre de votre film" class="form-group">
          <div id="TypeResearch" class="form-group divGenre">
            <?php
            $genrelist = getGenres($connexion);
            foreach ($genrelist as $key => $value){ ?>
              <label for=""><input type="checkbox" name="Genre[]" value=<?php echo str_replace(" ", "",$value['nom_genre'])?>> <?php echo $value['nom_genre'] ?> </label>
            <?php } ?>
          </div>
          <input type="submit" value="Rechercher" class="form-group">
        </form>
        <li>
          <li class="nav-item liNav"> <a href="gere.php" class="btn btn-primary">Gérer Films</a> </li>
        </ul>
      </header>
  <body>
    <p>
      <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseAddFilm" aria-expanded="false" aria-controls="collapseAddFilm">
        Ajouter un film
      </button>
    </p>
    <div class="collapse" id="collapseAddFilm">
      <form class="card card-body form-group" action="../src/addFilm.php" method="post">
        <div class="form-group">
          <label for="inputTitleO">Titre Original :</label>
          <input type="text" id="inputTitleO" name="titre_original" placeholder="Le titre original de votre film" class="form-control">
        </div>
        <div class="form-group">
          <label for="inputTitleF">Titre Français :</label>
          <input type="text" id="inputTitleF" name="titre_francais" placeholder="Le titre français de votre film" class="form-control">
        </div>
        <div class="form-group">
          <label for="inputNReal">Nom realisateur :</label>
          <input type="text" id="inputNReal" name="NReal" placeholder="Le nom du real de votre film" class="form-control">
        </div>
        <div class="form-group">
          <label for="inputPReal">Prenom realisateur :</label>
          <input type="text" id="inputPReal" name="PReal" placeholder="Le prenom du real de votre film" class="form-control">
        </div>
        <div class="form-group">
          <label for="inputPays">Pays du film :</label>
          <input type="text" id="inputPays" name="pays" placeholder="Le pays d'origine de votre film" class="form-control">
        </div>
        <div class="form-group">
          <label for="inputDate">Date du film :</label>
          <input type="text" id="inputDate" name="date" placeholder="La date de naissance de votre film" class="form-control">
        </div>
        <div class="form-group">
          <label for="inputDuree">Duree :</label>
          <input type="text" id="inputDuree" name="duree" placeholder="La durée de votre film" class="form-control">
        </div>
        <div id="colorGroup" class="form-group">
          <label for="colorGroup">Noir et blanc ?</label>
          <div class="form-check">
            <input type="radio" id="inputCheckNoir" name="couleur" class="form-check-input" value="Noir et Blanc">
            <label for="inputCheckNoir" class="form-check-label">Noir et blanc</label>
          </div>
          <div class="form-check">
            <input type="radio" id="inputCheckBlanc" name="couleur" class="form-check-input" value="couleur">
            <label for="inputCheckBlanc" class="form-check-label">Couleur</label>
          </div>
        </div>
        <label for="genreGroup">Genre :</label>
        <div id="genreGroup" class="form-group card card-body" style="max-height:300px; min-height:300px; max-width: 20em; overflow:scroll;">
          <?php
          $genrelist = getGenres($connexion);
          foreach ($genrelist as $key => $value){ ?>
            <div class="form-check card-text" style="">
              <input class="form-check-input" type="checkbox" name="nom_genre[]" id=<?php echo "inputCheckGenre".$key; ?> value=<?php echo str_replace(" ", "",$value['nom_genre'])?>>
              <label class="form-check-label" for=<?php echo "inputCheckGenre".$key; ?>>  <?php echo $value['nom_genre'] ?> </label>
            </div>
          <?php } ?>
        </div>
        <div class="form-group">
          <label for="inputFile">File :</label>
          <input type="text" id="inputFile" name="image" placeholder="" class="form-control">
        </div>
        <input class="btn btn-primary" type="submit" name="sub" value="Fini">
      </form>
    </div>
    <!-- Partie Supp -->
    <p>
      <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseDelFilm" aria-expanded="false" aria-controls="collapseDelFilm">
        Supprimer un film
      </button>
    </p>
    <?php $classDivSup = "";
    $_POST['title'] = $_POST['title'] ?? null;
    if ($_POST['title'] != null) {
      $classDivSup = "show";
    } ?>
    <div class="collapse <?php echo $classDivSup ?>" id="collapseDelFilm">
      <form class="form-group" action="gere.php" method="post">
        <input type="text" class="form-control" name="title" value="" placeholder="Nom du film (pour être plus rapide)">
      </form>
      <form class="card card-body form-group" action="../src/delFilm.php" method="post">
        <label for="filmGroup">Films :</label>
        <div id="filmGroup" class="form-group card card-body" style="max-height:300px; min-height:300px; max-width: 200em; overflow:scroll;">
          <?php
          $filmlist = rechercheFilms($connexion, $_POST['title'] ?? null, null, null, null);
          if (!is_null($filmlist)) {

          foreach ($filmlist as $key => $value){ ?>
            <div class="form-check card-text" style="">
              <input class="form-check-input" type="radio" name="id" id=<?php echo "inputRadioFilm".$key; ?> value=<?php echo str_replace(" ", "",$value['code_film'])?>>
              <label class="form-check-label" for=<?php echo "inputRadioFilm".$key; ?>>  <?php echo $value['titre_original']." ou ".$value['titre_francais'] ?> </label>
            </div>
          <?php
        }}else {
          echo " : il n'y a pas de films correspondant à votre recherche";
        } ?>
        </div>
        <input type="submit" name="" value="Supprimer !">
      </form>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
